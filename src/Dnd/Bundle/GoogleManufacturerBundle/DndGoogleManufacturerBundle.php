<?php

namespace Dnd\Bundle\GoogleManufacturerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DndGoogleManufacturerBundle
 *
 * @category  Class
 * @package   Dnd\Bundle\GoogleManufacturerBundle
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */
class DndGoogleManufacturerBundle extends Bundle
{
}
